# Bayesian Reasoning with Deep-Learned Knowledge

This repository belongs to the paper "Bayesian Reasoning with Deep-Learned Knowledge" (https://arxiv.org/abs/2001.11031).

It illustrates how to combine a deep generative model trained on handwritten digits with a deep convolutional network for digit classification to conditionally sample from the generator via Bayesian reasoning.

# Installation

Make sure that you have a python version >= 3.8. Then install the python packages scipy, matplotlib, nifty, and tensorflow.

```
pip3 install --user "scipy>=1.4" matplotlib "tensorflow==2.2" git+https://gitlab.mpcdf.mpg.de/ift/nifty@ccbf5012d7800ea1bef04cc8759912b40fddd899 git+https://gitlab.mpcdf.mpg.de/ift/nifty_hmc@6726c329116ba47c0370a2420f20b96ddfb22eda
```
Optionally for MPI parallelization:
```
pip3 install --user mpi4py
```
Sample your favourite digit (from 0 to 9) with the following command, e.g.:
The first argument corresponds to the digit, the second determines the number of HMC chains.
```
python3 demo.py 1 2
```

Optionally, but recomended, compute chains in parallel via MPI:

```
mpirun python3 demo.py 1 4 -nthreads=4
```
After the warmup phase, samples, effective sample size and R_hat are plotted and saved. This updates every few sampling steps. The code does not need to run fully and can be interrupted at any time, as the qualitative result no longer changes.
